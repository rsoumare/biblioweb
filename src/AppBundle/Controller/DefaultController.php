<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Admin\LivreBundle\Entity\Livre;

class DefaultController extends Controller
{

    /**
     * Lists all livre entities.
     *
     * @Route("/", name="livre_index_front")
     * @Method("GET")
     */
    public function livreAction()
    {
        $em = $this->getDoctrine()->getManager();

        $livres = $em->getRepository('LivreBundle:Livre')->findAll();

        return $this->render('default/index.html.twig', array(
            'livres' => $livres,
        ));
    }




}
