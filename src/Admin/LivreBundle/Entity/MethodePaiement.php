<?php

namespace Admin\LivreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MethodePaiement
 *
 * @ORM\Table(name="methode_paiement")
 * @ORM\Entity(repositoryClass="Admin\LivreBundle\Repository\MethodePaiementRepository")
 */
class MethodePaiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="troc", type="boolean")
     */
    private $troc;

    /**
     * @var string
     *
     * @ORM\Column(name="don", type="string", length=255)
     */
    private $don;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer")
     */
    private $prix;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set troc
     *
     * @param boolean $troc
     *
     * @return MethodePaiement
     */
    public function setTroc($troc)
    {
        $this->troc = $troc;

        return $this;
    }

    /**
     * Get troc
     *
     * @return bool
     */
    public function getTroc()
    {
        return $this->troc;
    }

    /**
     * Set don
     *
     * @param string $don
     *
     * @return MethodePaiement
     */
    public function setDon($don)
    {
        $this->don = $don;

        return $this;
    }

    /**
     * Get don
     *
     * @return string
     */
    public function getDon()
    {
        return $this->don;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return MethodePaiement
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return int
     */
    public function getPrix()
    {
        return $this->prix;
    }
}

